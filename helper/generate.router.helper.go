package helper

import (
	"net/http"
	"strings"
)

type key int

const (
	ParamsKey key = iota
)

func MatchRoute(pattern, path string) (bool, map[string]string) {
	patternParts := strings.Split(pattern, "/")
	pathParts := strings.Split(path, "/")

	if len(patternParts) != len(pathParts) {
		return false, nil
	}

	params := make(map[string]string)
	for i, part := range patternParts {
		if strings.HasPrefix(part, ":") {
			params[part[1:]] = pathParts[i]
		} else if part != pathParts[i] {
			return false, nil
		}
	}

	return true, params
}

func GetParams(req *http.Request) map[string]string {
	if params, ok := req.Context().Value(ParamsKey).(map[string]string); ok {
		return params
	}
	return nil
}
