Run DB
```azure
    1. docker compose -p ${npm_config_name} up -d
    2. cat dump.sql | docker exec -i ${npm_config_id} psql -U postgres -d ${npm_config_db}
```

Run Apps
```azure
    1. go mod download
    2. go run main.go
```

Test Apps
```azure
    1. open postman
    2. import go-native-boilerplate.postman.json
    3. run collection
```

#Objective
 - [x] CRUD for both Post and Tag
 - [x] Use go standard lib and github.com/lib/pq.
 - [x] The many-to-many relationship can be tricky. That's part of the assessment.
 - [ ] Deploy on cloud and can be tested through that.
 - [x] Document how to run on local development environtment.
 - [ ] GET search Post by tag {host_url}/api/posts/?tag={label}
 - [ ] Add authentication and authorization for user and administrator. User only drafting post.
    Admin only can publish post
 - [ ] Add unit test for each endpoint
 - [ ] Add integration test for each endpoint
 - [ ] Add CI/CD pipeline
 - [ ] Add monitoring and logging

#Tech Stack
 - Go
 - Postgres
 - Docker
 - Postman