package database

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	_config "github.com/rizkydwicmt/go-native/config"
)

func PgConnect(config _config.Postgres, appName string) (*sql.DB, error) {
	dbSource := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		config.Host,
		config.Port,
		config.User,
		config.Password,
		config.Database,
	)
	db, err := sql.Open("postgres", dbSource)

	if err != nil {
		return nil, err
	}

	err = db.Ping()

	if err != nil {
		return nil, err
	}

	return db, err
}
