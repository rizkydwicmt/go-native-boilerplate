package middleware

import (
	"github.com/rizkydwicmt/go-native/utilities/logger"
	"net/http"
)

type Middleware func(http.HandlerFunc) http.HandlerFunc

func LogMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logger.Http.Println("Request URI:", r.RequestURI)
		next(w, r)
	}
}
