package repository

import (
	"database/sql"
	"github.com/lib/pq"
	"github.com/rizkydwicmt/go-native/repository/tag/entity"
	"github.com/rizkydwicmt/go-native/service/post/model"
	"time"
)

type repository struct {
	db *sql.DB
}

func NewRepository(db *sql.DB) *repository {
	return &repository{db}
}

func (r *repository) Create(u model.Tag, author int) (*model.Tag, error) {
	tag := entity.NewTag(u)
	tag.CreatedAt = time.Now()
	SQL := `INSERT INTO "tags" (lable, created_at, created_by) VALUES ($1, $2, $3) RETURNING id`
	err := r.db.QueryRow(SQL, tag.Label, tag.CreatedAt, author).Scan(&u.ID)
	if err != nil {
		return nil, err
	}
	return &u, nil
}

func (r *repository) CreateMulti(tx *sql.Tx, tags []model.Tag, author int) ([]*model.Tag, error) {
	var createdTags []*model.Tag
	SQL := `INSERT INTO "tags" (label, created_at, created_by) VALUES ($1, $2, $3) RETURNING id`

	for i := range tags {
		tag := entity.NewTag(tags[i])
		tag.CreatedAt = time.Now()

		err := tx.QueryRow(SQL, tag.Label, tag.CreatedAt, author).Scan(&tags[i].ID)
		if err != nil {
			return nil, err
		}

		createdTags = append(createdTags, &tags[i])
	}

	return createdTags, nil
}

func (r *repository) FindOrCreateLabels(labels []string, author int) ([]*model.Tag, error) {
	tx, err := r.db.Begin()
	if err != nil {
		return nil, err
	}

	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}
		err = tx.Commit()
	}()

	SQL := `SELECT id, label FROM "tags" WHERE label = ANY($1)`
	rows, err := tx.Query(SQL, pq.Array(labels))
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var foundLabels = make(map[string]struct{})
	var tags []*model.Tag
	for rows.Next() {
		var tag entity.Tag
		if err := rows.Scan(&tag.ID, &tag.Label); err != nil {
			return nil, err
		}
		foundLabels[tag.Label] = struct{}{}
		var tagModel = tag.ToModel()
		tags = append(tags, &tagModel)
	}

	var tagsToCreate []model.Tag
	for _, label := range labels {

		if _, found := foundLabels[label]; !found {
			tagsToCreate = append(tagsToCreate, model.Tag{Label: label})
		}
	}

	var createdTags []*model.Tag
	if len(tagsToCreate) > 0 {
		createdTags, err = r.CreateMulti(tx, tagsToCreate, author)

		if err != nil {
			return nil, err
		}
		tags = append(tags, createdTags...)
	}

	return tags, nil
}
