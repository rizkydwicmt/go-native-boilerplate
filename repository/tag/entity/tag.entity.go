package entity

import (
	"github.com/rizkydwicmt/go-native/service/post/model"
	"time"
)

type Tag struct {
	ID        int       `db:"id"`
	Label     string    `db:"label"`
	CreatedAt time.Time `db:"created_at"`
	CreatedBy int       `db:"created_by"`
	UpdatedAt time.Time `db:"updated_at"`
	UpdatedBy int       `db:"updated_by"`
}

func NewTag(m model.Tag) Tag {
	return Tag{
		ID:        m.ID,
		Label:     m.Label,
		CreatedAt: m.CreatedAt,
		UpdatedAt: m.UpdatedAt,
	}
}

func (e Tag) ToModel() model.Tag {
	return model.Tag{
		ID:        e.ID,
		Label:     e.Label,
		CreatedAt: e.CreatedAt,
		UpdatedAt: e.UpdatedAt,
	}
}
