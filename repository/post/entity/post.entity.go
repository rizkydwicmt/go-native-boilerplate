package entity

import (
	"github.com/rizkydwicmt/go-native/service/post/model"
	"time"
)

type Post struct {
	ID          int       `db:"id"`
	Title       string    `db:"title"`
	Content     string    `db:"content"`
	IsPublished bool      `db:"is_published"`
	CreatedAt   time.Time `db:"created_at"`
	CreatedBy   int       `db:"created_by"`
	UpdatedAt   time.Time `db:"updated_at"`
	UpdatedBy   int       `db:"updated_by"`
}

func NewPost(m model.Post) Post {
	return Post{
		ID:          m.ID,
		Title:       m.Title,
		Content:     m.Content,
		IsPublished: m.IsPublished,
	}
}

func (e Post) ToModel() model.Post {
	return model.Post{
		ID:          e.ID,
		Title:       e.Title,
		Content:     e.Content,
		IsPublished: e.IsPublished,
		CreatedAt:   e.CreatedAt,
		UpdatedAt:   e.UpdatedAt,
	}
}
