package repository

import (
	"database/sql"
	"github.com/rizkydwicmt/go-native/repository/post/entity"
	"github.com/rizkydwicmt/go-native/service/post/model"
	"time"
)

type repository struct {
	db *sql.DB
}

func NewRepository(db *sql.DB) *repository {
	return &repository{db}
}

func (r *repository) Create(u model.Post, author int) (*model.Post, error) {
	post := entity.NewPost(u)
	post.CreatedAt = time.Now()
	post.UpdatedAt = time.Now()
	SQL := `INSERT INTO "posts" (title, content, is_published, created_at, updated_at, created_by) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`
	err := r.db.QueryRow(SQL, post.Title, post.Content, post.IsPublished, post.CreatedAt, post.UpdatedAt, author).Scan(&u.ID)
	if err != nil {
		return nil, err
	}
	return &u, nil
}

func (r *repository) Update(u model.Post, author int) (*model.Post, error) {
	post := entity.NewPost(u)
	post.UpdatedAt = time.Now()
	SQL := `UPDATE "posts" SET title=$1, content=$2, is_published=$3, updated_at=$4, updated_by=$5 WHERE id=$6 RETURNING id`
	err := r.db.QueryRow(SQL, post.Title, post.Content, post.IsPublished, post.UpdatedAt, author, u.ID).Scan(&u.ID)
	if err != nil {
		return nil, err
	}
	return &u, nil
}

func (r *repository) Delete(id int) error {
	SQL := `DELETE FROM "posts" WHERE id=$1`
	_, err := r.db.Exec(SQL, id)
	return err
}

func (r *repository) FindAll() ([]model.Post, error) {
	SQL := `SELECT id, title, content, is_published, created_at, updated_at FROM "posts"`
	rows, err := r.db.Query(SQL)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var posts []model.Post
	for rows.Next() {
		var post entity.Post
		err := rows.Scan(&post.ID, &post.Title, &post.Content, &post.IsPublished, &post.CreatedAt, &post.UpdatedAt)
		if err != nil {
			return nil, err
		}
		posts = append(posts, post.ToModel())
	}
	if err = rows.Err(); err != nil {
		return posts, err
	}
	return posts, nil
}

func (r *repository) FindByID(id int) (*model.Post, error) {
	SQL := `SELECT id, title, content, is_published, created_at, updated_at FROM "posts" WHERE id=$1`
	row := r.db.QueryRow(SQL, id)
	var post entity.Post
	err := row.Scan(&post.ID, &post.Title, &post.Content, &post.IsPublished, &post.CreatedAt, &post.UpdatedAt)
	if err != nil {
		return nil, err
	}
	u := post.ToModel()
	return &u, nil
}

func (r *repository) FindMyPost(author int) ([]model.Post, error) {
	SQL := `SELECT id, title, content, is_published, created_at, updated_at FROM "posts" WHERE created_by=$1`
	rows, err := r.db.Query(SQL, author)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var posts []model.Post
	for rows.Next() {
		var post entity.Post
		err := rows.Scan(&post.ID, &post.Title, &post.Content, &post.IsPublished, &post.CreatedAt, &post.UpdatedAt)
		if err != nil {
			return nil, err
		}
		posts = append(posts, post.ToModel())
	}
	if err = rows.Err(); err != nil {
		return posts, err
	}
	return posts, nil
}

func (r *repository) FindMyPostById(id int, author int) (*model.Post, error) {
	SQL := `SELECT id, title, content, is_published, created_at, updated_at FROM "posts" WHERE id=$1 AND created_by=$2`
	row := r.db.QueryRow(SQL, id, author)
	var product entity.Post
	err := row.Scan(&product.ID, &product.Title, &product.Content, &product.IsPublished, &product.CreatedAt, &product.UpdatedAt)
	if err != nil {
		return nil, err
	}
	u := product.ToModel()
	return &u, nil
}

func (r *repository) UpdateMyPost(u model.Post, author int) (*model.Post, error) {
	post := entity.NewPost(u)
	post.UpdatedAt = time.Now()
	SQL := `UPDATE "posts" SET title=$1, content=$2, is_published=$3, updated_at=$4, updated_by=$5 WHERE id=$6 AND created_by=$7 RETURNING id`
	err := r.db.QueryRow(SQL, post.Title, post.Content, post.IsPublished, post.UpdatedAt, author, post.ID, author).Scan(&u.ID)
	if err != nil {
		return nil, err
	}
	return &u, nil
}

func (r *repository) DeleteMyPost(id int, author int) error {
	SQL := `DELETE FROM "posts" WHERE id=$1 AND created_by=$2`
	_, err := r.db.Exec(SQL, id, author)
	return err
}
