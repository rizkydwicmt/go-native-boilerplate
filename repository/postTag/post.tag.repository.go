package repository

import (
	"database/sql"
	entity "github.com/rizkydwicmt/go-native/repository/postTag/enitity"
	"github.com/rizkydwicmt/go-native/service/post/model"
)

type repository struct {
	db *sql.DB
}

func NewRepository(db *sql.DB) *repository {
	return &repository{db}
}

func (r *repository) CreateMulti(u []model.PostTag) error {
	SQL := `INSERT INTO "post_tags" (post_id, tag_id) VALUES ($1, $2)`

	for i := range u {
		postTag := entity.NewPostTag(u[i])
		err := r.db.QueryRow(SQL, postTag.PostID, postTag.TagID)
		if err != nil {
			continue
		}
	}

	return nil
}

func (r *repository) DeleteByPost(postId int) error {
	SQL := `DELETE FROM "post_tags" WHERE post_id=$1`
	_, err := r.db.Exec(SQL, postId)
	return err
}

func (r *repository) DeleteByTags(tagId int) error {
	SQL := `DELETE FROM "post_tags" WHERE tag_id=$1`
	_, err := r.db.Exec(SQL, tagId)
	return err
}

func (r *repository) FindByPost(postId int) ([]model.PostTag, error) {
	SQL := `SELECT post_id, tag_id FROM "post_tags" WHERE post_id=$1`
	rows, err := r.db.Query(SQL, postId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var postTags []model.PostTag
	for rows.Next() {
		var postTag entity.PostTag
		err := rows.Scan(&postTag.PostID, &postTag.TagID)
		if err != nil {
			return nil, err
		}
		postTags = append(postTags, postTag.ToModel())
	}

	return postTags, nil
}
