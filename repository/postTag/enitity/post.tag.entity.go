package entity

import "github.com/rizkydwicmt/go-native/service/post/model"

type PostTag struct {
	PostID int `db:"post_id"`
	TagID  int `db:"tag_id"`
}

func NewPostTag(m model.PostTag) PostTag {
	return PostTag{
		PostID: m.PostID,
		TagID:  m.TagID,
	}
}

func (e PostTag) ToModel() model.PostTag {
	return model.PostTag{
		PostID: e.PostID,
		TagID:  e.TagID,
	}
}
