package handler

import (
	"context"
	"database/sql"
	productHandler "github.com/rizkydwicmt/go-native/handler/post"
	"github.com/rizkydwicmt/go-native/helper"
	"github.com/rizkydwicmt/go-native/utilities/logger"
	"github.com/rizkydwicmt/go-native/utilities/middleware"
	"net/http"
)

type router struct {
	routes map[string]map[string]http.HandlerFunc
}

func NewRouter(db *sql.DB) *router {
	router := &router{
		routes: make(map[string]map[string]http.HandlerFunc),
	}

	router.addRoute("GET", "/check-health", func(res http.ResponseWriter, req *http.Request) {
		helper.WriteToResponseBody(res, "OK", "Success")
	})
	router.Group("/post", []middleware.Middleware{middleware.LogMiddleware}, productHandler.RegisterRoutes(db))
	router.printRoutes()

	return router
}

func (r *router) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	for path, handlers := range r.routes {
		if matched, params := helper.MatchRoute(path, req.URL.Path); matched {
			if handler, methodExists := handlers[req.Method]; methodExists {
				ctx := context.WithValue(req.Context(), helper.ParamsKey, params)
				handler(w, req.WithContext(ctx))
				return
			}
		}
	}
	http.NotFound(w, req)
}

func (r *router) addRoute(method, path string, handler http.HandlerFunc) {
	if r.routes[path] == nil {
		r.routes[path] = make(map[string]http.HandlerFunc)
	}
	r.routes[path][method] = handler
}

func (r *router) Group(basePath string, middlewares []middleware.Middleware, routes func(addRoute func(method, path string, handler http.HandlerFunc))) {
	groupAddRoute := func(method, path string, handler http.HandlerFunc) {
		fullPath := basePath + path
		if fullPath[len(fullPath)-1] == '/' {
			fullPath = fullPath[:len(fullPath)-1]
		}
		if len(middlewares) > 0 {
			for _, middleware := range middlewares {
				handler = middleware(handler)
			}
		}
		r.addRoute(method, fullPath, handler)
	}
	routes(groupAddRoute)
}

func (r *router) printRoutes() {
	logger.Http.Println("===== Registered Routes =====")
	for path, methods := range r.routes {
		for method := range methods {
			logger.Http.Println(method, path)
		}
	}
	logger.Http.Println("===== End Registered Routes =====")
}
