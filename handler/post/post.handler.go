package handler

import (
	"database/sql"
	_postRepo "github.com/rizkydwicmt/go-native/repository/post"
	_postTag "github.com/rizkydwicmt/go-native/repository/postTag"
	_tagRepo "github.com/rizkydwicmt/go-native/repository/tag"
	_service "github.com/rizkydwicmt/go-native/service/post"
	"net/http"
)

func RegisterRoutes(db *sql.DB) func(addRoute func(method, path string, handler http.HandlerFunc)) {
	postRepo := _postRepo.NewRepository(db)
	tagRepo := _tagRepo.NewRepository(db)
	postTagRepo := _postTag.NewRepository(db)
	service := _service.NewService(postRepo, tagRepo, postTagRepo)
	return func(addRoute func(method, path string, handler http.HandlerFunc)) {
		addRoute("GET", "/", service.GetAllPost)
		addRoute("GET", "/:id", service.GetPostById)
		addRoute("POST", "/", service.CreatePost)
		addRoute("PUT", "/:id", service.UpdatePost)
		addRoute("DELETE", "/:id", service.DeletePost)
	}
}
