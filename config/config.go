package config

import (
	"time"

	"github.com/spf13/viper"
)

type Service struct {
	Name         string        `validate:"required" mapstructure:"name"`
	Env          string        `validate:"required" mapstructure:"environment"`
	Addr         string        `validate:"required" mapstructure:"address"`
	IdleTimeout  time.Duration `validate:"required" mapstructure:"idle_timeout"`
	ReadTimeout  time.Duration `validate:"required" mapstructure:"read_timeout"`
	WriteTimeout time.Duration `validate:"required" mapstructure:"write_timeout"`
}

type Postgres struct {
	Host      string `validate:"required" mapstructure:"host"`
	Port      int    `validate:"required" mapstructure:"port"`
	Database  string `validate:"required" mapstructure:"database"`
	User      string `validate:"required" mapstructure:"user"`
	Password  string `validate:"required" mapstructure:"password"`
	MaxIdle   int    `validate:"required" mapstructure:"max_idle"`
	MaxActive int    `validate:"required" mapstructure:"max_active"`
}

type Config struct {
	Service  Service  `mapstructure:"service"`
	Postgres Postgres `mapstructure:"postgres"`
}

const DEFAULT_PATH_CONFIG = "./config/config.yaml"

func Init(staticPathConfig string) (Config, error) {
	var config Config
	viper.SetConfigFile(staticPathConfig)
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		return config, err
	}

	err = viper.Unmarshal(&config)
	if err != nil {
		return config, err
	}

	return config, nil
}
