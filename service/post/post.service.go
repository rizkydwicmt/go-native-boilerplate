package service

import (
	"github.com/rizkydwicmt/go-native/helper"
	"github.com/rizkydwicmt/go-native/service/post/model"
	"github.com/rizkydwicmt/go-native/service/post/repository"
	postRequest "github.com/rizkydwicmt/go-native/service/post/request"
	"net/http"
	"strconv"
)

type Service interface {
	CreatePost(writer http.ResponseWriter, request *http.Request)
	UpdatePost(writer http.ResponseWriter, request *http.Request)
	DeletePost(writer http.ResponseWriter, request *http.Request)
	GetAllPost(writer http.ResponseWriter, request *http.Request)
	GetPostById(writer http.ResponseWriter, request *http.Request)
}

type service struct {
	postRepo    repository.Post
	tagRepo     repository.Tag
	postTagRepo repository.PostTag
}

func NewService(postRepo repository.Post, tagRepo repository.Tag, postTagRepo repository.PostTag) *service {
	return &service{postRepo, tagRepo, postTagRepo}
}

func (s *service) GetAllPost(writer http.ResponseWriter, request *http.Request) {
	posts, err := s.postRepo.FindAll()
	if err != nil {
		helper.WriteErrToResponseBody(writer, err, "Failed to get posts")
		return
	}
	helper.WriteToResponseBody(writer, posts, "Success get posts")
}

func (s *service) GetPostById(writer http.ResponseWriter, request *http.Request) {
	params := helper.GetParams(request)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		helper.WriteErrToResponseBody(writer, err, "Failed to get post")
		return
	}
	posts, err := s.postRepo.FindByID(id)
	if err != nil {
		helper.WriteNotFoundToResponseBody(writer, "", "Post not found!")
		return
	}
	helper.WriteToResponseBody(writer, posts, "Success get post by id")

}

func (s *service) CreatePost(writer http.ResponseWriter, request *http.Request) {
	postsRequest := postRequest.CreatePostRequest{}
	err := helper.ReadFromRequestBody(request, &postsRequest)
	if err != nil {
		helper.WriteErrToResponseBody(writer, err, "Failed to create post")
		return
	}

	postModel, label := postsRequest.ToModel()

	//Create post
	post, err := s.postRepo.Create(postModel, 1)
	if err != nil {
		helper.WriteErrToResponseBody(writer, err, "Failed to create post")
		return
	}

	// Create or find tags
	tags, err := s.tagRepo.FindOrCreateLabels(label, 1)
	if err != nil {
		helper.WriteErrToResponseBody(writer, err, "Failed to create post")
		return
	}

	// Assign tags to post
	postTags := model.NewPostTagsFromModel(*post, tags)
	err = s.postTagRepo.CreateMulti(postTags)
	if err != nil {
		helper.WriteErrToResponseBody(writer, err, "Failed to create post")
		return
	}

	helper.WriteToResponseBody(writer, map[string]interface{}{
		"id": post.ID,
	}, "Success create post")
}

func (s *service) UpdatePost(writer http.ResponseWriter, request *http.Request) {
	params := helper.GetParams(request)
	postsRequest := postRequest.CreatePostRequest{}
	err := helper.ReadFromRequestBody(request, &postsRequest)
	if err != nil {
		helper.WriteErrToResponseBody(writer, err, "Failed to update post")
		return
	}
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		helper.WriteErrToResponseBody(writer, err, "Failed to update post")
		return
	}

	postModel, label := postsRequest.ToModel()
	postModel.ID = id

	_, err = s.postRepo.Update(postModel, 1)
	if err != nil {
		helper.WriteErrToResponseBody(writer, err, "Failed to update post")
		return
	}

	// Create or find tags
	tags, err := s.tagRepo.FindOrCreateLabels(label, 1)
	if err != nil {
		helper.WriteErrToResponseBody(writer, err, "Failed to update post")
		return
	}

	// Assign tags to post
	postTags := model.NewPostTagsFromModel(postModel, tags)
	err = s.postTagRepo.CreateMulti(postTags)
	if err != nil {
		helper.WriteErrToResponseBody(writer, err, "Failed to update post")
		return
	}

	helper.WriteToResponseBody(writer, map[string]interface{}{
		"id": postModel.ID,
	}, "Success update post")
}

func (s *service) DeletePost(writer http.ResponseWriter, request *http.Request) {
	params := helper.GetParams(request)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		helper.WriteErrToResponseBody(writer, err, "Failed to delete post")
		return
	}
	err = s.postTagRepo.DeleteByPost(id)
	if err != nil {
		helper.WriteErrToResponseBody(writer, err, "Failed to delete post")
		return
	}
	err = s.postRepo.DeleteMyPost(id, 1)
	if err != nil {
		helper.WriteErrToResponseBody(writer, err, "Failed to delete post")
		return
	}
	helper.WriteToResponseBody(writer, map[string]interface{}{
		"id": id,
	}, "Success delete post")
}
