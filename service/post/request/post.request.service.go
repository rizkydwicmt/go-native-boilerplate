package request

import "github.com/rizkydwicmt/go-native/service/post/model"

type CreatePostRequest struct {
	ID          int      `json:"id"`
	Title       string   `json:"title"`
	Content     string   `json:"content"`
	IsPublished bool     `json:"isPublished"`
	Tags        []string `json:"tags"`
}

func (r CreatePostRequest) ToModel() (model.Post, []string) {
	return model.Post{
		ID:          r.ID,
		Title:       r.Title,
		Content:     r.Content,
		IsPublished: r.IsPublished,
	}, r.Tags
}
