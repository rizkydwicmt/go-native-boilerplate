package repository

import "github.com/rizkydwicmt/go-native/service/post/model"

type Post interface {
	Create(u model.Post, author int) (*model.Post, error)
	Update(u model.Post, author int) (*model.Post, error)
	UpdateMyPost(u model.Post, author int) (*model.Post, error)
	Delete(id int) error
	DeleteMyPost(id int, author int) error
	FindAll() ([]model.Post, error)
	FindByID(id int) (*model.Post, error)
	FindMyPost(author int) ([]model.Post, error)
	FindMyPostById(id int, author int) (*model.Post, error)
}
