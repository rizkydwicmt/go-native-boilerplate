package repository

import "github.com/rizkydwicmt/go-native/service/post/model"

type PostTag interface {
	CreateMulti(u []model.PostTag) error
	FindByPost(postId int) ([]model.PostTag, error)
	DeleteByPost(postId int) error
	DeleteByTags(tagId int) error
}
