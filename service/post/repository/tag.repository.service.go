package repository

import (
	"github.com/rizkydwicmt/go-native/service/post/model"
)

type Tag interface {
	Create(u model.Tag, author int) (*model.Tag, error)
	FindOrCreateLabels(labels []string, author int) ([]*model.Tag, error)
}
