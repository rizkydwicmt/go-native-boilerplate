package model

type PostTag struct {
	PostID int `json:"postId"`
	TagID  int `json:"tagId"`
}

func NewPostTags(postID int, tagIDs []int) []PostTag {
	var postTags []PostTag
	for _, tagID := range tagIDs {
		postTags = append(postTags, PostTag{PostID: postID, TagID: tagID})
	}
	return postTags
}

func NewPostTagsFromModel(p Post, tags []*Tag) []PostTag {
	var postTags []PostTag
	for _, tag := range tags {
		getValue := &tag.ID
		postTags = append(postTags, PostTag{PostID: p.ID, TagID: *getValue})
	}
	return postTags
}
