package model

import "time"

type Tag struct {
	ID        int    `json:"id"`
	Label     string `json:"label"`
	CreatedAt time.Time
	UpdatedAt time.Time
}
