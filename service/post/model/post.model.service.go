package model

import "time"

type Post struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Content     string `json:"content"`
	IsPublished bool   `json:"isPublished"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
}
