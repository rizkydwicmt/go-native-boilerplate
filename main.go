package main

import (
	"context"
	"flag"
	"fmt"
	_config "github.com/rizkydwicmt/go-native/config"
	"github.com/rizkydwicmt/go-native/handler"
	_database "github.com/rizkydwicmt/go-native/utilities/db"
	"github.com/rizkydwicmt/go-native/utilities/logger"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	filePath := flag.String("config", _config.DEFAULT_PATH_CONFIG, "-config <PATH>")
	config, err := _config.Init(*filePath)
	if err != nil {
		logger.Error.Println(err)
		panic(err)
	}

	pg, err := _database.PgConnect(config.Postgres, "PostgreDB")

	if err != nil {
		logger.Error.Println(err)
		panic(err)
	}

	defer func() {
		pg.Close()
	}()

	router := handler.NewRouter(pg)
	server := &http.Server{
		Addr:         config.Service.Addr,
		Handler:      router,
		IdleTimeout:  config.Service.IdleTimeout * time.Minute,
		ReadTimeout:  config.Service.ReadTimeout * time.Minute,
		WriteTimeout: config.Service.WriteTimeout * time.Minute,
	}

	go func() {
		logger.Http.Println("========= Server Started =========")
		logger.Http.Println("=========", config.Service.Addr, "=========")
		if err := server.ListenAndServe(); err != nil {
			logger.Http.Println(err)
			logger.Http.Println("========= Server Ended =========")
		}
	}()

	sigChan := make(chan os.Signal)
	done := make(chan bool, 1)
	signal.Notify(sigChan, os.Interrupt, os.Kill, syscall.SIGTERM, syscall.SIGTSTP, syscall.SIGINT)

	go func() {
		sig := <-sigChan
		fmt.Println()
		fmt.Println(sig)
		done <- true
	}()
	<-done
	logger.Error.Println("Received terminate, graceful shutdown", sigChan)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_ = server.Shutdown(ctx)
	time.Sleep(1 * time.Second)
}
